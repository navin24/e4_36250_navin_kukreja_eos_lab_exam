#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/un.h>
#include"common.h"

#define MY_PATH "/home/sunbeam/E4_36250_Navin_Kukreja_EOS_Lab_Exam/Exam/desd_sock"
#define ITEMS_DB "items.db"

void scan_item(edevices_t *edev)
{
	edevices_t edev;;
	int fp = open(ITEMS_DB,"rb");
	if(fp < 0)
    {
        perror("file failed to open.\n ")
        _exit(14);
    }
	printf("Enter item details : \n");
	printf("Enter Model : ");
	scanf("%*c%[^\n]s", edev->mod);
	printf("Enter Company : ");
	scanf("%f", edev->company);
	printf("Enter Category : ");
	scanf("%d", edev->devices);
    printf("Enter Price : ");
	scanf("%d", &edev->price);
	printf("Type (1-Stationary, 2-Grocery, 3-Cosmetic) : ");
	scanf("%d", (int *)&edev->devices);
}

static char *tostring(dev1_t type)
{
	switch(type)
	{
		case MOBILE: return "Mobile"; break;
		case TV: return "TV"; break;
		case WASHING_MACHINE: return "Washing Machine"; break;
	}
	return NULL;
}


 
int main()
{
    int ret,cli_fd;
    struct sockaddr_un ser_addr;
    char buf[512];
    dev1_t devicess;
    edevices_t edev;


    cli_fd=socket(AF_UNIX,SOCK_STREAM,0);
    if(cli_fd<0)
    {
        perror("cli_socket() failed.\n");
        _exit(4);
    }
    printf("Client socket created.\n");

    printf("client is connecting to server socket.\n");
    ser_addr.sun_family = AF_UNIX;
    strcpy(ser_addr.sun_path, MY_PATH);
    ret = connect(cli_fd, (struct sockaddr*)&ser_addr, sizeof(ser_addr));
        if(ret<0)
        {
            perror("connection() failed.\n");
            _exit(5);
        }
        printf("client is connected to server socket: %d\n", ret);

        
        do {
            printf("Client: ");
            scan_item(edev);
            fgets(buf, sizeof(buf), stdin);
            write(cli_fd, buf, strlen(buf)+1);

            read(cli_fd, buf, sizeof(buf));
            printf("Client message: %s", buf);
        }while(strcmp(buf, "bye\n") != 0);

        close(cli_fd);
        printf("client (%d) closed socket connection.\n",cli_fd);

    return 0;
}
