#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/un.h>
#include"common.h"

#define MY_PATH "/home/sunbeam/E4_36250_Navin_Kukreja_EOS_Lab_Exam/Exam/desd_sock"
void add_item(edevices_t edev)
{
    int fp;

	fp = open(ITEMS_DB, "ab");

 	write(&edev, sizeof(edev), 1, fp);
	printf("Item is added in file\n");

 	close(fp);
 }
 void print_item(edevices_t edev)
 {
     printf("%-10s %-10s %-10%s %-10s" , "Model","Comapny","Category","Price" )
 	printf("%-2d %-20s %-20s %-10s %-.2f\n",edev.mod, edev.comapny, tostring(edev.devicess), edev.prices);
 }

 void print_items(void)
 {
 	int fp;
 	edevices_t edev;

 	fp = open(ITEMS_DB, "rb");

 	while((read(&edev, sizeof(edev), 1, fp)) !> 0)
 		print_item(edev);

 	close(fp);
 }
 void find_item(void)
 {
    char find_dev[30];
    printf("Enter model you want to search:");

    scanf("%s",find_dev);
 	int fp;
 	edevices_t edev;

 	fp = open(ITEMS_DB, "rb");

	while((read(&edev, sizeof(edev), 1, fp)) > 0)
 	{
 		if(strcmp(edev.mod,find_dev) == 0)
 			print_item(edev);
        else
        {
            printf("Item not found.\n");
        }
 	}
 	close(fp);
 }

 void edit_item(void)
 {
 	int fp;
 	edevices_t edev;
 	fp = open(ITEMS_DB, "rb+");
	
 	while((read(&edev, sizeof(edev), 1, fp)) > 0)
	{
        if(strcmp(edev.mod,mod)==0)
		printf("Enter new model : ");
		scanf("%s", edev.mod);
        printf("Enter new price :   ");
        scanf("%f",&edev.price);
		write(&edev, sizeof(edev), 1, fp);
		break;
	}
 	close(fp);
 }

void delete_item(char mod[])
{
 	int fp;
 	FILE *tempfp;
 	edevices_t edev;

 	fp = open(ITEMS_DB, "rb");
 	tempfp = open("temp.db", "ab");
	
 	while((read(&edev, sizeof(edev), 1, fp)) > 0)
 	{
		if(strcmp(edev.mod,mod)!=0)
 			write(&item, sizeof(item), 1, tempfp);
 	}

 	close(fp);
 	close(tempfp);

 	remove(ITEMS_DB);
 	rename("temp.db", ITEMS_DB);

 }

int main()
{
    int ret,ser_fd,cli_fd;
    struct sockaddr_un ser_addr,cli_addr;
    socklen_t sock_len;
    char buf[512];
    dev1_t devicess;
    edevices_t edev;

    ser_fd=socket(AF_UNIX,SOCK_STREAM,0);
    if(ser_fd<0)
    {
        perror("ser_socket() failed.\n");
        _exit(1);
    }
    printf("Server socket created.\n");

    unlink(MY_PATH);

    ser_addr.sun_family = AF_UNIX;
    strcpy(ser_addr.sun_path, MY_PATH);
    ret = bind(ser_fd,(struct sockaddr*)&ser_addr,sizeof(ser_addr));
    if(ret<0)
    {
        perror("Addr not assigned.\n");
        _exit(2);
    }
    printf("Server addres given.\n");

    ret=listen(ser_fd, 5);
    if(ret<0)
    {
        perror("listen() failed.\n");
        _exit(6);
    }
    printf("listening to server socket.\n");

    printf("server is waiting for client connection.\n");
    sock_len = sizeof(cli_addr);
    cli_fd = accept(ser_fd, (struct sockaddr*)&cli_addr, &sock_len);
    printf("server accepted client connection: %d\n", cli_fd);

    do {
        read(cli_fd, buf, sizeof(buf));
        printf("Client: %s", buf);

        printf(" Server message  : ");
        fgets(buf, sizeof(buf), stdin);
        write(cli_fd, buf, strlen(buf)+1);
    }while(strcmp(buf, "bye\n") != 0);

    close(cli_fd);
    printf("Server closed client socket connection.\n");

    shutdown(ser_fd, SHUT_RDWR);
    printf("server closed main socket socket.\n");

    return 0;
}
